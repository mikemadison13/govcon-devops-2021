# Govcon Devops 2021

Code repo used as a demonstration during Mike Madison's Lightning Round: Setting Up All the DevOps Technologies for a Drupal Project talk at Drupal Govcon 2021.

## Prerequisites

This repository assumes you have the basics for a Lando stack installed, including PHP, Composer, Docker Deskop, Lando, Git, etc. See https://mikemadison.net/blog/2020/7/21/setting-up-a-new-macbook-pro-for-local-development for more info.

## Getting Started With This Repository

Clone the code use `git clone`

Install the composer and npm dependencies using `composer install`

Provision the Lando containers using `lando start`

Install Drupal 9 using `lando composer site-install`
